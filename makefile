# Run 'make' to compile the executable calc_test
#
# Dependencies:
# - Google Test development library. You can install it using your package manager
#   or run 'make build-thirdparty' to install it locally (requires cmake).

# the compiler: gcc for C program, define as g++ for C++
CXX = g++

# compiler flags:
#  -g     - this flag adds debugging information to the executable file
#  -Wall  - this flag is used to turn on most compiler warnings
CPPFLAGS  = -g -Wall -std=c++14 -I./thirdparty/include
LDLIBS = -L./thirdparty/lib -lgtest

all: calc_test

# Clean all build files, including thirdparty
cleanall: clean clean-thirdparty
 
calc_test: calc_test.o calc.o
	$(CXX) -o calc_test calc_test.o calc.o $(LDLIBS)

calc_test.o: calc_test.cpp
	$(CXX) $(CPPFLAGS) -c calc_test.cpp

calc.o: calc.cpp calc.hpp
	$(CXX) $(CPPFLAGS) -c calc.cpp

# Remove build files
clean:
	$(RM) calc_test *.o


######### Build gtest if not installed on the system

# Name of the directory containing gtest source files (must be in ./thirdparty/src/)
GTEST_SRC=googletest-1.14.0

# Install gtest in the thirdparty directory
build-thirdparty:
	cd thirdparty/src/$(GTEST_SRC) && cmake -S. -B_build -DCMAKE_INSTALL_PREFIX=../.. && cd _build && make install

# Clean up gtest installation and build files in thirdparty
clean-thirdparty:
	cd thirdparty && $(RM) -r include lib src/$(GTEST_SRC)/_build