# The `calc` project 

This project implements functionalities of  a simple  calculator.

## Test suite

Unit tests are implemented in `calc_tes.cpp`

## Prerequisites 

Google Test (`gtest`) must be installed on your system.
You can install it using your package manager or running `make build-thirdparty` in the current directory (for this, cmake is required).

## Installing 

In order to compile the test suite, run `make`
